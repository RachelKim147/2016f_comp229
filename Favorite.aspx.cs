﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Favorite : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            BindFavorites();
    }

    protected void BindFavorites()
    {
        try
        {
            gv_favorites.DataSource = GetRecipes();
            gv_favorites.DataKeyNames = new string[] { "RecipeID" };
            gv_favorites.DataBind();
        }
        catch (Exception error)
        {
            lbResult.Text = error.Message;
        }
    }

    protected List<Recipe> GetRecipes()
    {
        List<Recipe> list = new List<Recipe>();

        try
        {
            WebAccount myInfo = WebAccountRepository.GetAccountByAccountName(User.Identity.Name);
            List<RecipeFavorite> favorites = RecipeFavoriteRepository.GetFavoritesByAccountID(myInfo.AccountID);
            foreach(RecipeFavorite favo in favorites)
            {
                list.Add(RecipeRepository.GetRecipeByRecipeID(favo.RecipeID));
            }
        }
        catch (Exception)
        {
            throw;
        }

        return list;
    }

    protected void lb_title_Click(object sender, EventArgs e)
    {
        int selectedValue = Convert.ToInt32((sender as LinkButton).CommandArgument);
        Response.Redirect(string.Format("RecipeDetails.aspx?selectedValue=" + selectedValue));
    }
}