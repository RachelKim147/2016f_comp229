﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuCntHolder" Runat="Server">
    <img src="images/search_title_image.jpg" width="1050" height="406" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="BodyCntHolder" Runat="Server">
    <p class="menuLabel">Search</p>
    <div>
        <asp:DropDownList ID="dd_submitter" runat="server" DataSourceID="od_submitter" DataTextField="Submitter" DataValueField="Submitter" AppendDataBoundItems="True">
            <asp:listitem text="All" value="all" />
        </asp:DropDownList>
        <asp:DropDownList ID="dd_category" runat="server" DataSourceID="od_category" DataTextField="CategoryName" DataValueField="CategoryID" AppendDataBoundItems="true">
            <asp:listitem text="All" value="-1" />
        </asp:DropDownList>
        <asp:DropDownList ID="dd_ingredient" runat="server" DataSourceID="od_ingredient" DataTextField="Name" DataValueField="Name" AppendDataBoundItems="True">
            <asp:listitem text="All" value="all" />
        </asp:DropDownList>
        <asp:Button ID="searchBT" runat="server" Text="Search" OnClick="searchBT_Click" />
        <br />
        <asp:ObjectDataSource ID="od_submitter" runat="server" SelectMethod="GetRecipesGroupBySubmitter" TypeName="SearchRepository"></asp:ObjectDataSource>
        <asp:ObjectDataSource ID="od_category" runat="server" SelectMethod="GetCategories" TypeName="CategoryRepository"></asp:ObjectDataSource>
        <asp:ObjectDataSource ID="od_ingredient" runat="server" SelectMethod="GetIngredientsGroupByName" TypeName="SearchRepository"></asp:ObjectDataSource>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        <br />
        <asp:GridView ID="gv_result" runat="server" AutoGenerateColumns="False" CssClass="mGrid">
            <Columns>
                <asp:TemplateField HeaderText="Name" SortExpression="Name" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lb_title" runat="server" Text='<%# Bind("Name") %>' CommandArgument='<%#Eval("RecipeID")%>' OnClick ="lb_title_Click"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Submitter" HeaderText="Writer" SortExpression="Submitter" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem" />
                <asp:BoundField DataField="CookingTime" HeaderText="Cooking Time (Hours)" SortExpression="CookingTime" DataFormatString="{0:f2}" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem" />               
            </Columns>
        </asp:GridView>
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BottomCntHolder" Runat="Server">
</asp:Content>

