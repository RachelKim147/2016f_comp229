﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void bt_login_Click(object sender, EventArgs e)
    {
        try
        {
            if (ValidateUser(tb_ID.Text, tb_password.Text))
            {
                FormsAuthentication.RedirectFromLoginPage(tb_ID.Text, false);
            }
        }
        catch (Exception error)
        {
            lbResult.Text = error.Message;
        }
    }

    protected void bt_logout_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();
        Response.Redirect("Default.aspx");
    }

    private bool ValidateUser(string userName, string passWord)
    {
        string lookupPassword = null;
        WebAccount foundInfo = new WebAccount();

        try
        {
            // Check for invalid userName.
            // userName must not be null and must be between 1 and 50 characters.
            if ((null == userName) || (0 == userName.Length) || (userName.Length > 50))
            {
                throw new Exception("Invalid User name!");
            }

            // Check for invalid passWord.
            // passWord must not be null and must be between 1 and 50 characters.
            if ((null == passWord) || (0 == passWord.Length) || (passWord.Length > 50))
            {
                throw new Exception("Invalid User password!");
            }

            try
            {
                foundInfo = WebAccountRepository.GetAccountByAccountName(userName);
                lookupPassword = foundInfo.AccountPassword;
            }
            catch (Exception)
            {
                throw;
            }

            // If no password found, return false.
            if (null == lookupPassword)
            {
                throw new Exception("Empty password!");
            }

            // Compare lookupPassword and input passWord, using a case-sensitive comparison.
            if (0 != string.Compare(lookupPassword, passWord, false))
            {
                throw new Exception("Wrong ID or Password!");
            }
        }
        catch (Exception)
        {
            throw;
        }

        return true;
    }
}