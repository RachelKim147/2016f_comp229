﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuCntHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyCntHolder" Runat="Server">
    <p class="menuLabel">Register</p>
    <table>
        <tr>
            <td>
                <asp:Label ID="lbID" runat="server" Text="ID:" CssClass="defaultStyle"></asp:Label></td>
            <td>
                <asp:TextBox ID="tbID" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="val_ID" runat="server" ErrorMessage="Put ID!" ControlToValidate="tbID" CssClass="addRecipeError" Display="Dynamic" EnableClientScript="False" ValidationGroup="register">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbPassword" runat="server" Text="Password:" CssClass="defaultStyle"></asp:Label></td>
            <td>
                <asp:TextBox ID="tbPassword" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_pw" runat="server" ErrorMessage="Put password!" ControlToValidate="tbPassword" CssClass="addRecipeError" EnableClientScript="False" ValidationGroup="register">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="btRegister" runat="server" Text="Register" OnClick="btRegister_Click" ValidationGroup="register"/>
            </td>
            <td>
                <asp:Button ID="btCancel" runat="server" Text="Cancel" OnClick="btCancel_Click" />
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="summary_Validation" runat="server" CssClass="addRecipeError" ValidationGroup="register" />
    <br />

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="BottomCntHolder" Runat="Server">
</asp:Content>

