﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Setup.aspx.cs" Inherits="Setup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuCntHolder" Runat="Server">
    <img src="images/setup_image.png" height="406" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyCntHolder" Runat="Server">
    <p class="menuLabel">Setup</p>
    <asp:Label ID="lb_hompageThemes" runat="server" Text="Select theme:" CssClass="setupBigText"></asp:Label>
    <br />
    <asp:dropdownlist id="ddlThemes" runat="server" autopostback="True" DataSourceID="ThemeObjectDataSource" DataTextField="Name" DataValueField="Name" OnDataBound="ddlThemes_DataBound" OnSelectedIndexChanged="ddlThemes_SelectedIndexChanged" >
    </asp:dropdownlist>
    <asp:ObjectDataSource ID="ThemeObjectDataSource" runat="server" SelectMethod="GetThemes" TypeName="ThemeRepository"></asp:ObjectDataSource>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="BottomCntHolder" Runat="Server">
</asp:Content>

