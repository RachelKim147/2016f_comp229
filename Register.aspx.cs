﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btRegister_Click(object sender, EventArgs e)
    {
        if (tbID.Text == "" || tbPassword.Text == "")
            return;

        try
        {
            WebAccountRepository.InsertAccount(tbID.Text, tbPassword.Text);
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception error)
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = error.Message;
            cv.ValidationGroup = "register";
            this.Page.Validators.Add(cv);      
        }
    }

    protected void btCancel_Click(object sender, EventArgs e)
    {
        tbID.Text = "";
        tbPassword.Text = "";
    }
}