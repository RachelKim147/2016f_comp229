﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="RecipeDetails.aspx.cs" Inherits="RecipeDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuCntHolder" Runat="Server">
    <img src="images/recipe_title_image.jpg" width="1050" height="406" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyCntHolder" Runat="Server">
    <p class="menuLabel">Recipe Details</p>
    <p class="addRecipeBigText">General information</p>
    <asp:CheckBox ID="cb_favorite" runat="server" Text="Favorite" BorderStyle="None" AutoPostBack="True" CssClass="favoriteCheckBox" OnCheckedChanged="cb_favorite_CheckedChanged" />
    <asp:DetailsView ID="dv_recipe" runat="server" AutoGenerateRows="False" Height="50px" Width="610px" OnModeChanging="dv_recipe_ModeChanging" CssClass="mGrid" OnItemDeleting="dv_recipe_ItemDeleting" OnItemUpdating="dv_recipe_ItemUpdating">
        <FieldHeaderStyle Width="130px" />
        <Fields>
            <asp:TemplateField HeaderText="Title" SortExpression="Title" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                <EditItemTemplate>
                    <asp:TextBox ID="tb_edit_title" runat="server" Text='<%# Bind("recipe_name") %>' Width="400px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="val_edit_title" runat="server" ErrorMessage="Please put new title!" Text="*" ControlToValidate="tb_edit_title" EnableClientScript="False" CssClass="addRecipeError"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tb_insert_title" runat="server" Text='<%# Bind("recipe_name") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lb_title" runat="server" Text='<%# Bind("recipe_name") %>'></asp:Label>
                </ItemTemplate>

<HeaderStyle CssClass="gridHeader"></HeaderStyle>

<ItemStyle CssClass="gridItem"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Submitter" SortExpression="Submitter" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                <EditItemTemplate>
                    <asp:TextBox ID="tb_edit_submitter" runat="server" Text='<%# Bind("submitter_name") %>' Width="400px" ReadOnly="True" Enabled="False"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tb_insert_submitter" runat="server" Text='<%# Bind("submitter_name") %>' ReadOnly="True" Enabled="False"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lb_submitter" runat="server" Text='<%# Bind("submitter_name") %>'></asp:Label>
                </ItemTemplate>

<HeaderStyle CssClass="gridHeader"></HeaderStyle>

<ItemStyle CssClass="gridItem"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Category" SortExpression="Category" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                <EditItemTemplate>
                    <asp:DropDownList ID="dd_edit_category" runat="server" DataSourceID="categoryObjectDS" DataTextField="CategoryName" DataValueField="CategoryID" SelectedValue=<%# Bind("category_id") %>>
                    </asp:DropDownList>
                    <br />
                    <asp:CheckBox ID="checkBox_edit_newCategory" runat="server" AutoPostBack="True" OnCheckedChanged="checkBox_edit_newCategory_CheckedChanged" Text="Add new category: " />
                    <asp:TextBox ID="tb_edit_newCategory" runat="server" Enabled="False" Width="400px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="val_newCategory" runat="server" Enabled="False" ErrorMessage="Put new category name!" Text="*" ControlToValidate="tb_edit_newCategory" EnableClientScript="False" CssClass="addRecipeError"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="dd_insert_category" runat="server" DataSourceID="categoryObjectDS" DataTextField="CategoryName" DataValueField="CategoryID" SelectedValue=<%# Bind("category_id") %> Enabled="False">
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="dd_category" runat="server" DataSourceID="categoryObjectDS" DataTextField="CategoryName" DataValueField="CategoryID" SelectedValue=<%# Bind("category_id") %> Enabled="False">
                    </asp:DropDownList>
                </ItemTemplate>

<HeaderStyle CssClass="gridHeader"></HeaderStyle>

<ItemStyle CssClass="gridItem"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CookingTime" SortExpression="CookingTime" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                <EditItemTemplate>
                    <asp:TextBox ID="tb_edit_time" runat="server" Text='<%# Bind("cooking_time") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tb_insert_time" runat="server" Text='<%# Bind("cooking_time") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lb_time" runat="server" Text='<%# Bind("cooking_time") %>'></asp:Label>
                </ItemTemplate>

<HeaderStyle CssClass="gridHeader"></HeaderStyle>

<ItemStyle CssClass="gridItem"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="NumOfServing" SortExpression="NumOfServing" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                <EditItemTemplate>
                    <asp:TextBox ID="tb_edit_serving" runat="server" Text='<%# Bind("number_servings") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tb_insert_serving" runat="server" Text='<%# Bind("number_servings") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lb_serving" runat="server" Text='<%# Bind("number_servings") %>'></asp:Label>
                </ItemTemplate>

<HeaderStyle CssClass="gridHeader"></HeaderStyle>

<ItemStyle CssClass="gridItem"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description" SortExpression="Description" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                <EditItemTemplate>
                    <asp:TextBox ID="tb_edit_description" runat="server" Text='<%# Bind("recipe_description") %>' Width="400px" TextMode="MultiLine" Height="145px"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tb_insert_description" runat="server" Text='<%# Bind("recipe_description") %>' Width="400px" TextMode="MultiLine" Height="145px"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:TextBox ID="tb_description" runat="server" Text='<%# Bind("recipe_description") %>' Width="400px" TextMode="MultiLine" Height="145px" ReadOnly="True"></asp:TextBox>
                </ItemTemplate>

<HeaderStyle CssClass="gridHeader"></HeaderStyle>

<ItemStyle CssClass="gridItem"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RecipeID" SortExpression="RecipeID" InsertVisible="False" ShowHeader="False" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("recipe_id") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("recipe_id") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("recipe_id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="categoryObjectDS" runat="server" SelectMethod="GetCategories" TypeName="CategoryRepository"></asp:ObjectDataSource>
    <asp:Label ID="lbResultRecipe" runat="server" CssClass="addRecipeError"></asp:Label>
    <br />
    <p class="addRecipeBigText">Ingredients information</p>
    <asp:DetailsView ID="dv_ingredients" runat="server" Height="50px" Width="610px" AllowPaging="True" AutoGenerateRows="False" CssClass="mGrid" OnModeChanging="dv_ingredients_ModeChanging" OnItemDeleting="dv_ingredients_ItemDeleting" OnItemUpdating="dv_ingredients_ItemUpdating" OnPageIndexChanging="dv_ingredients_PageIndexChanging" OnItemInserting="dv_ingredients_ItemInserting">
        <FieldHeaderStyle Width="130px" />
        <Fields>
            <asp:TemplateField HeaderText="ingredient_id" InsertVisible="False" ShowHeader="False" SortExpression="ingredient_id" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ingredient name" SortExpression="Name" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                <EditItemTemplate>
                    <asp:TextBox ID="tb_edit_name" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tb_insert_name" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lb_name" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Quantity" SortExpression="Quantity" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                <EditItemTemplate>
                    <asp:TextBox ID="tb_edit_quantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tb_insert_quantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lb_quantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Measure" SortExpression="Measure" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                <EditItemTemplate>
                    <asp:TextBox ID="tb_edit_measure" runat="server" Text='<%# Bind("Measure") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tb_insert_measure" runat="server" Text='<%# Bind("Measure") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lb_meause" runat="server" Text='<%# Bind("Measure") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
    <asp:Label ID="lbResultIngredient" runat="server" CssClass="addRecipeError"></asp:Label>
    <br />
    <asp:ValidationSummary ID="valSummary" runat="server" CssClass="addRecipeError" />
    <br />
    </asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="BottomCntHolder" Runat="Server">
</asp:Content>

