﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddRecipe : BasePage
{
    List<IngredientUserControl> ingredients;

    protected void Page_Init(object sender, EventArgs e)
    {
        ingredients = new List<IngredientUserControl>();
        SearchUserControls(Page.Controls);

        if(!Page.IsPostBack)
        {
            try
            {
                tb_submittedBy.Text = User.Identity.Name;

            }
            catch (Exception ex)
            {
                CustomValidator cv = new CustomValidator();
                cv.IsValid = false;
                cv.ErrorMessage = ex.Message;
                cv.ValidationGroup = "submit";
                this.Page.Validators.Add(cv);
            }
        }
    }

    public void SearchUserControls(ControlCollection controls)
    {
        foreach (Control ctl in controls)
        {
            if (ctl is IngredientUserControl)
            {
                ingredients.Add(ctl as IngredientUserControl);
            }

            if (ctl.Controls.Count > 0)
                SearchUserControls(ctl.Controls);
        }
    }


    protected void bt_save_Click(object sender, EventArgs e)
    {
        List<Ingredient> newIngredients = new List<Ingredient>();

        foreach(Control ctl in ingredients)
        {
            IngredientUserControl castComp = ctl as IngredientUserControl;
            if( castComp != null && castComp.bActivate )
            {
                double quantity = 0;
                try
                {
                    quantity = Convert.ToDouble(castComp.quantity);
                }
                catch (Exception ex)
                {
                    CustomValidator cv = new CustomValidator();
                    cv.IsValid = false;
                    cv.ErrorMessage = String.Format("{0} : {1}", castComp.ID, ex.Message);
                    cv.ValidationGroup = "submit";
                    this.Page.Validators.Add(cv);
                }
                
                if (string.Equals(castComp.nameOfIngredient, "All", StringComparison.CurrentCultureIgnoreCase))
                {
                    CustomValidator cv = new CustomValidator();
                    cv.IsValid = false;
                    cv.ErrorMessage = String.Format("{0} : {1}", castComp.ID, "You can't use 'all' as your ingredients' name!");
                    cv.ValidationGroup = "submit";
                    this.Page.Validators.Add(cv);
                }

                Ingredient newIngredient = new Ingredient { Name = castComp.nameOfIngredient, Quantity = quantity, Measure = castComp.unitOfMeasure };
                newIngredients.Add(newIngredient);
            }      
        }

        if( newIngredients.Count == 0 )
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = "You have to at least one ingredient!";
            cv.ValidationGroup = "submit";
            this.Page.Validators.Add(cv);
        }
        else if( string.Equals(tb_submittedBy.Text, "All", StringComparison.CurrentCultureIgnoreCase) )
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = "You can't use 'all' as your name!";
            cv.ValidationGroup = "submit";
            this.Page.Validators.Add(cv);
        }
        else
        {
            try
            {
                int categoryID = -1;

                if (check_newCategory.Checked)
                {
                    categoryID = CategoryRepository.InsertCategory(tb_newCategory.Text);
                }
                else
                {
                    categoryID = Convert.ToInt32(dd_category.SelectedValue);
                }

                Recipe newItem = new Recipe
                {
                    Title = tb_nameOfRecipe.Text,
                    Submitter = tb_submittedBy.Text,
                    Category = categoryID,
                    CookingTime = Convert.ToDouble(tb_cookingTime.Text),
                    NumOfServing = Convert.ToInt32(tb_numOfServing.Text),
                    Description = tb_description.Text
                };

                int createdID = RecipeRepository.InsertRecipe(newItem);
                if ( createdID > 0)
                {
                    IngredientRepository.InsertIngredients(createdID, newIngredients);
                    Response.Redirect("Recipes.aspx");
                }

            }
            catch (Exception ex)
            {
                CustomValidator cv = new CustomValidator();
                cv.IsValid = false;
                cv.ErrorMessage = ex.Message;
                cv.ValidationGroup = "submit";
                this.Page.Validators.Add(cv);
            }
        }   
    }

    protected void br_cancel_Click(object sender, EventArgs e)
    {
        foreach (Control ctl in ingredients)
        {
            IngredientUserControl castComp = ctl as IngredientUserControl;
            if (castComp != null && castComp.bActivate)
            {
                castComp.bActivate = false;
            }
        }

        tb_nameOfRecipe.Text = "";
        tb_submittedBy.Text = "";
        tb_cookingTime.Text = "";
        tb_description.Text = "";
        tb_numOfServing.Text = "";

        dd_category.SelectedIndex = 0;
    }

    protected void check_newCategory_CheckedChanged(object sender, EventArgs e)
    {
        if(check_newCategory.Checked)
        {
            dd_category.Enabled = false;
            tb_newCategory.Enabled = true;
            val_newCategory.Enabled = true;
        }
        else
        {
            dd_category.Enabled = true;
            tb_newCategory.Text = "";
            tb_newCategory.Enabled = false;
            val_newCategory.Enabled = false;
        }
    }
}