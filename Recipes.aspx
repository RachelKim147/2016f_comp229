﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="Recipes.aspx.cs" Inherits="Recepies" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuCntHolder" Runat="Server">
       <img src="images/recipe_title_image.jpg" width="1050" height="406" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="BodyCntHolder" Runat="Server">
    <p class="menuLabel">Recipes</p>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="recipeObjectDataSource" CssClass="mGrid">
        <Columns>
            <asp:TemplateField HeaderText="Title" SortExpression="Title" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="lb_title" runat="server" Text='<%# Eval("Title") %>' CommandArgument='<%#Eval("RecipeID") %>' OnClick ="lb_title_Click"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Submitter" HeaderText="Writer" SortExpression="Submitter" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem" />
            <asp:BoundField DataField="CookingTime" HeaderText="Cooking Time (Hours)" SortExpression="CookingTime" DataFormatString="{0:f1}" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="recipeObjectDataSource" runat="server" SelectMethod="GetRecipes" TypeName="RecipeRepository"></asp:ObjectDataSource>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BottomCntHolder" Runat="Server">
</asp:Content>

