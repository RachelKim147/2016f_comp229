﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IngredientUserControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void CheckEmpty(Object s, ServerValidateEventArgs e)
    {
        if ( cb_ingredient.Checked && e.Value == "" )
        {
            e.IsValid = false;
        }
        else if ( cb_ingredient.Checked == false && e.Value != "" )
        {
            e.IsValid = false;
        }
    }

    public bool bActivate
    {
        set
        {
            cb_ingredient.Checked = value;

            tb_name.Enabled = value;
            tb_quantity.Enabled = value;
            tb_unitOfMeasure.Enabled = value;

            if( cb_ingredient.Checked == false )
            {
                nameOfIngredient = " ";
                quantity = " ";
                unitOfMeasure = " ";
            }
        }
        get { return cb_ingredient.Checked;  }
    }

    public String nameOfIngredient
    {
        set { tb_name.Text = value; }
        get { return tb_name.Text; }
    }

    public String quantity
    {
        set { tb_quantity.Text = value; }
        get { return tb_quantity.Text; }
    }

    public String unitOfMeasure
    {
        set { tb_unitOfMeasure.Text = value; }
        get { return tb_unitOfMeasure.Text; }
    }

    protected void cb_ingredient_CheckedChanged(object sender, EventArgs e)
    {
        tb_name.Enabled = cb_ingredient.Checked;
        tb_quantity.Enabled = cb_ingredient.Checked;
        tb_unitOfMeasure.Enabled = cb_ingredient.Checked;

        if (cb_ingredient.Checked == false)
        {
            nameOfIngredient = "";
            quantity = "";
            unitOfMeasure = "";
        }
    }
}