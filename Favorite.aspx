﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Favorite.aspx.cs" Inherits="Favorite" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuCntHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyCntHolder" Runat="Server">
    <p class="menuLabel">Favorites</p>
    <asp:GridView ID="gv_favorites" runat="server" AutoGenerateColumns="False" CssClass="mGrid">
        <Columns>
                <asp:TemplateField HeaderText="Title" SortExpression="Title" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lb_title" runat="server" Text='<%# Bind("Title") %>' CommandArgument='<%#Eval("RecipeID")%>' OnClick ="lb_title_Click"></asp:LinkButton>
                    </ItemTemplate>

<HeaderStyle CssClass="gridHeader"></HeaderStyle>

<ItemStyle CssClass="gridItem"></ItemStyle>
                </asp:TemplateField>
                <asp:BoundField DataField="Submitter" HeaderText="Writer" SortExpression="Submitter" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem" >
<HeaderStyle CssClass="gridHeader"></HeaderStyle>

<ItemStyle CssClass="gridItem"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="CookingTime" HeaderText="Cooking Time (Hours)" SortExpression="CookingTime" DataFormatString="{0:f2}" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem" >               
<HeaderStyle CssClass="gridHeader"></HeaderStyle>

<ItemStyle CssClass="gridItem"></ItemStyle>
                </asp:BoundField>
            </Columns>
    </asp:GridView>
    <br />
    <asp:Label ID="lbResult" runat="server" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="BottomCntHolder" Runat="Server">
</asp:Content>

