﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuCntHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyCntHolder" Runat="Server">
    <p class="menuLabel">Login</p>
    <asp:Label ID="lb_ID" runat="server" Text="ID:" CssClass="defaultStyle"></asp:Label>
    <br />
    <asp:TextBox ID="tb_ID" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="val_ID" runat="server" ErrorMessage="Put your ID!" ControlToValidate="tb_ID" CssClass="addRecipeError" Display="Dynamic" EnableClientScript="False" ValidationGroup="login">*</asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Label ID="lb_password" runat="server" Text="Password:" CssClass="defaultStyle"></asp:Label>
    <br />
    <asp:TextBox ID="tb_password" runat="server" TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="val_password" runat="server" ErrorMessage="Put your password!" ControlToValidate="tb_password" CssClass="addRecipeError" Display="Dynamic" EnableClientScript="False" ValidationGroup="login">*</asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Button ID="bt_login" runat="server" Text="Login" OnClick="bt_login_Click" ValidationGroup="login" />
    <asp:Button ID="bt_logout" runat="server" Text="Logout" OnClick="bt_logout_Click" />
    <br />
    <asp:Label ID="lbRegister" runat="server" Text="If you don't register yet, " CssClass="defaultStyle"></asp:Label> <asp:LinkButton ID="btRegister" runat="server" CssClass="linkButtonText" PostBackUrl="~/Register.aspx">click here</asp:LinkButton>
    <br />
    <br />
    <asp:Label ID="lbResult" runat="server" CssClass="addRecipeError"></asp:Label>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="BottomCntHolder" Runat="Server">
</asp:Content>

