﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Setup : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ddlThemes_DataBound(object sender, EventArgs e)
    {
        ddlThemes.SelectedValue = Page.Theme;
    }

    protected void ddlThemes_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Don't put Request.Cookies[]! It's Response.Cookies[]!
        if (Response.Cookies["myTheme"] != null)
        {
            Response.Cookies["myTheme"].Value = ddlThemes.SelectedValue;
            Response.Cookies["myTheme"].Expires = DateTime.Now.AddDays(1);
        }

        Response.Redirect("Setup.aspx");
    }
}