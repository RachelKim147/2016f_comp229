﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AccountInfo : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack == false)
        {
            try
            {
                WebAccount myInfo = WebAccountRepository.GetAccountByAccountName(User.Identity.Name);

                tbID.Text = myInfo.AccountName;
                tbPassword.Text = myInfo.AccountPassword;
            }
            catch (Exception error)
            {
                CustomValidator cv = new CustomValidator();
                cv.IsValid = false;
                cv.ValidationGroup = "unregister";
                cv.ErrorMessage = error.Message;
                this.Page.Validators.Add(cv);
            }
        }
    }

    protected void btLogout_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();
        Response.Redirect("Default.aspx");
    }

    protected void btUnregister_Click(object sender, EventArgs e)
    {
        try
        {
            WebAccount myInfo = WebAccountRepository.GetAccountByAccountName(User.Identity.Name);
            WebAccountRepository.DeleteAccount(myInfo.AccountID, tbPassword.Text);
        }
        catch (Exception error)
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ValidationGroup = "unregister";
            cv.ErrorMessage = error.Message;
            this.Page.Validators.Add(cv);
        }
        finally
        {
            FormsAuthentication.SignOut();
            Response.Redirect("Default.aspx");
        }
    }

    protected void btSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            WebAccount myInfo = WebAccountRepository.GetAccountByAccountName(User.Identity.Name);
            WebAccountRepository.UpdateAccount(myInfo.AccountID, tbPassword.Text);
        }
        catch(Exception error)
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ValidationGroup = "submit";
            cv.ErrorMessage = error.Message;
            this.Page.Validators.Add(cv);
        }
    }
}