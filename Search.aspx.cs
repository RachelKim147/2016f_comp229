﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Search : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void searchBT_Click(object sender, EventArgs e)
    {
        int categoryType;

        int.TryParse(dd_category.SelectedValue, out categoryType);

        gv_result.DataSource = SearchRepository.GetSearchResult(dd_submitter.SelectedValue, categoryType, dd_ingredient.SelectedValue);

        gv_result.DataBind();
    }

    protected void lb_title_Click(object sender, EventArgs e)
    {
        int selectedValue = Convert.ToInt32((sender as LinkButton).CommandArgument);
        Response.Redirect(string.Format("RecipeDetails.aspx?selectedValue=" + selectedValue));
    }
}