﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Recepies : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void lb_title_Click(object sender, EventArgs e)
    {
        int selectedValue = Convert.ToInt32((sender as LinkButton).CommandArgument);
        Response.Redirect(string.Format("RecipeDetails.aspx?selectedValue=" + selectedValue));
    }
    
}