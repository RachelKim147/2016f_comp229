﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IngredientUserControl.ascx.cs" Inherits="IngredientUserControl" %>
<asp:CheckBox ID="cb_ingredient" runat="server" CausesValidation="True" AutoPostBack="True" OnCheckedChanged="cb_ingredient_CheckedChanged" Text="Ingredient: " CssClass="addRecipeMediumText"/>
<asp:TextBox ID="tb_name" runat="server" Enabled="False" ValidationGroup="submit"></asp:TextBox>
<asp:Label ID="lb_quantity" runat="server" Text="Quantity: " CssClass="addRecipeMediumText"></asp:Label>
<asp:TextBox ID="tb_quantity" runat="server" Enabled="False" ValidationGroup="submit"></asp:TextBox>
<asp:Label ID="lb_measure" runat="server" Text="Unit of measure: " CssClass="addRecipeMediumText"></asp:Label>
<asp:TextBox ID="tb_unitOfMeasure" runat="server" Enabled="False" ValidationGroup="submit"></asp:TextBox>
<br />
<asp:CustomValidator ID="val_name" runat="server" ControlToValidate="tb_name" EnableClientScript="False" ErrorMessage="Fill the name of ingredient!" OnServerValidate="CheckEmpty" Display="Dynamic" ValidateEmptyText="True" ValidationGroup="submit" CssClass="addRecipeError"></asp:CustomValidator>
<asp:CustomValidator ID="val_quantity" runat="server" ControlToValidate="tb_quantity" EnableClientScript="False" ErrorMessage="Fill the quantity of ingredient!" OnServerValidate="CheckEmpty" Display="Dynamic" ValidateEmptyText="True" ValidationGroup="submit" CssClass="addRecipeError"></asp:CustomValidator>
<asp:RangeValidator ID="rVal_quantity" runat="server" ErrorMessage="Fill correct quantity(Number only)!" ControlToValidate="tb_quantity" Display="Dynamic" EnableClientScript="False" MaximumValue="999999999.99" MinimumValue="1.0" ValidationGroup="submit" CssClass="addRecipeError" Type="Double"></asp:RangeValidator>
<asp:CustomValidator ID="val_measure" runat="server" ControlToValidate="tb_unitOfMeasure" EnableClientScript="False" ErrorMessage="Fill the measure!!" OnServerValidate="CheckEmpty" Display="Dynamic" ValidateEmptyText="True" ValidationGroup="submit" CssClass="addRecipeError"></asp:CustomValidator>
