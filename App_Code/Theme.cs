﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Theme
/// </summary>
public class Theme
{
    private string name;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    // Constructor
    public Theme(string newName)
    {
        Name = newName;
    }
}