﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Oracle.ManagedDataAccess.Types;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.ManagedDataAccess.Client;

/// <summary>
/// Summary description for CategoryRepository
/// </summary>
public class CategoryRepository
{
    public CategoryRepository()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static List<Category> GetCategories()
    {
        List<Category> list = new List<Category>();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return list;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection( connectionString );
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT * FROM categories";

        DataTable table = new DataTable();

        try
        {
            // Open the data connection
            comm.Connection.Open();
            // Excute the command and save the result in a DataTanble
            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                Category newItem = new Category( Convert.ToInt32(row["category_id"]), row["category_name"].ToString());
                list.Add(newItem);
            }

            // Close the reader
            reader.Close();
        }
        catch (Exception e)
        {
            // [Todo] How can I handle this error?
            string error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }

        return list;
    }

    public static Category GetCategoriesByCategoryId(int categoryId)
    {
        Category newItem = new Category();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return newItem;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT * FROM categories WHERE category_id = :categoryID";

        OracleParameter param = new OracleParameter("categoryID", OracleDbType.Int32);
        param.Value = categoryId;
        comm.Parameters.Add(param);

        DataTable table = new DataTable();

        try
        {
            // Open the data connection
            comm.Connection.Open();
            // Excute the command and save the result in a DataTanble
            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                newItem.CategoryID = Convert.ToInt32(row["category_id"]);
                newItem.CategoryName = row["category_name"].ToString();
            }

            // Close the reader
            reader.Close();
        }
        catch (Exception e)
        {
            // [Todo] How can I handle this error?
            string error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }

        return newItem;
    }

    public static int InsertCategory(string newCategory)
    {
        int createdID = -1;

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return createdID;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandText = "insertCategory";

        OracleParameter returnParam = new OracleParameter("createdid", OracleDbType.Int32);
        returnParam.Direction = ParameterDirection.ReturnValue;
        comm.Parameters.Add(returnParam);

        OracleParameter param = new OracleParameter("categoryName", OracleDbType.Varchar2);
        param.Value = newCategory;
        comm.Parameters.Add(param);

        try
        {
            // Open the data connection
            comm.Connection.Open();

            comm.ExecuteNonQuery();

            int.TryParse(returnParam.Value.ToString(), out createdID);
        }
        catch (Exception e)
        {
            // [Todo] How can I handle this error?
            string error = e.Message;
            throw;
        }
        finally
        {
            conn.Close();
        }

        return createdID;
    }
}