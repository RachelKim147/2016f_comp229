﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : System.Web.UI.Page
{
    public BasePage()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        HttpCookie userCookie;
        userCookie = Request.Cookies["myTheme"];
        if (userCookie == null)
        {
            userCookie = new HttpCookie("myTheme", "Dark"); // default dark
            userCookie.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(userCookie);

            Page.Theme = userCookie.Value;
        }
        else
        {
            Page.Theme = userCookie.Value;
        }
    }
}