﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IngredientRepository
/// </summary>
public class IngredientRepository
{
    public IngredientRepository()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void DeleteIngredientByIngredientID(int ingredientID)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "DELETE FROM ingredients WHERE ingredient_id = :ingredientID";

        OracleParameter param = new OracleParameter("ingredientID", OracleDbType.Int32);
        param.Value = ingredientID;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }
    }

    public static void InsertIngredients(int recipeID, List<Ingredient> ingredients)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        foreach (Ingredient newItem in ingredients)
        {
            comm.CommandType = CommandType.StoredProcedure;
            comm.CommandText = "InsertIngredient";

            comm.Parameters.Clear();

            OracleParameter param = new OracleParameter("recID", OracleDbType.Int32);
            param.Value = recipeID;
            comm.Parameters.Add(param);

            param = new OracleParameter("ingName", OracleDbType.Varchar2);
            param.Value = newItem.Name;
            comm.Parameters.Add(param);

            param = new OracleParameter("quan", OracleDbType.Double);
            param.Value = newItem.Quantity;
            comm.Parameters.Add(param);

            param = new OracleParameter("msr", OracleDbType.Varchar2);
            param.Value = newItem.Measure;
            comm.Parameters.Add(param);

            try
            {
                comm.Connection.Open();
                comm.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }
    }

    public static void InsertIngredient(int recipeID, Ingredient ingredient)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();


        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandText = "InsertIngredient";

        comm.Parameters.Clear();

        OracleParameter param = new OracleParameter("recID", OracleDbType.Int32);
        param.Value = recipeID;
        comm.Parameters.Add(param);

        param = new OracleParameter("ingName", OracleDbType.Varchar2);
        param.Value = ingredient.Name;
        comm.Parameters.Add(param);

        param = new OracleParameter("quan", OracleDbType.Double);
        param.Value = ingredient.Quantity;
        comm.Parameters.Add(param);

        param = new OracleParameter("msr", OracleDbType.Varchar2);
        param.Value = ingredient.Measure;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }
    }

    public static void UpdateIngredient(Ingredient updateIngredient)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "UPDATE ingredients SET ingredient_name = :name, quantity = :quantity, measure = :measure WHERE ingredient_id = :id";

        OracleParameter param = new OracleParameter("name", OracleDbType.Varchar2);
        param.Value = updateIngredient.Name;
        comm.Parameters.Add(param);

        param = new OracleParameter("quantity", OracleDbType.Int32);
        param.Value = updateIngredient.Quantity;
        comm.Parameters.Add(param);

        param = new OracleParameter("measure", OracleDbType.Varchar2);
        param.Value = updateIngredient.Measure;
        comm.Parameters.Add(param);

        param = new OracleParameter("id", OracleDbType.Int32);
        param.Value = updateIngredient.IngredientID;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }
    }

    public static List<Ingredient> GetIngredientsByRecipeID(int recipeID)
    {
        List<Ingredient> list = new List<Ingredient>();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return list;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT * FROM ingredients WHERE recipe_id = :recipeID";

        OracleParameter param = new OracleParameter("reciperID", OracleDbType.Int32);
        param.Value = recipeID;
        comm.Parameters.Add(param);

        DataTable table = new DataTable();

        try
        {
            comm.Connection.Open();
            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                Ingredient newItem = new Ingredient
                {
                    IngredientID = Convert.ToInt32(row["ingredient_id"]),
                    Name = row["ingredient_name"].ToString(),
                    Quantity = Convert.ToDouble(row["quantity"]),
                    Measure = row["measure"].ToString(),
                };
                list.Add(newItem);
            }
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }

        return list;
    }
}