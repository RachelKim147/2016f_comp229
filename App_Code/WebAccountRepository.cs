﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WebAccountRepository
/// </summary>
public class WebAccountRepository
{
    public WebAccountRepository()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static WebAccount GetAccountByAccountName(string name)
    {
        WebAccount info = new WebAccount();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return info;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT * FROM ACCOUNTS WHERE account_name = :accountName";

        OracleParameter param = new OracleParameter("accountName", OracleDbType.Varchar2);
        param.Value = name;
        comm.Parameters.Add(param);

        DataTable table = new DataTable();

        try
        {
            comm.Connection.Open();

            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                info.AccountID = Convert.ToInt32(row["account_id"]);
                info.AccountName = row["account_name"].ToString();
                info.AccountPassword = row["account_password"].ToString();
            }

            reader.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }

        return info;
    }

    public static int InsertAccount(string accName, string accPassword)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return -1;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandText = "InsertAccount";

        comm.Parameters.Clear();

        // Return value (First)
        OracleParameter returnParam = new OracleParameter("createdID", OracleDbType.Decimal);
        returnParam.Direction = ParameterDirection.ReturnValue;
        comm.Parameters.Add(returnParam);

        // Function parameters (Next)
        OracleParameter param = new OracleParameter("accountName", OracleDbType.Varchar2);
        param.Value = accName;
        comm.Parameters.Add(param);

        param = new OracleParameter("accountPassword", OracleDbType.Varchar2);
        param.Value = accPassword;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }

        int returnVal = 0;
        if (returnParam.Value != null)
        {
            int.TryParse(returnParam.Value.ToString(), out returnVal);
        }

        return returnVal;
    }

    public static void UpdateAccount(int accountID, string password)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "UPDATE accounts SET account_password = :accountPassword WHERE account_id = :accountID";

        OracleParameter param = new OracleParameter("accountPassword", OracleDbType.Varchar2);
        param.Value = password;
        comm.Parameters.Add(param);

        param = new OracleParameter("accountID", OracleDbType.Int32);
        param.Value = accountID;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }
    }

    public static void DeleteAccount(int accountID, string password)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "DELETE FROM accounts WHERE account_password = :accountPassword AND account_id = :accountID";

        OracleParameter param = new OracleParameter("accountPassword", OracleDbType.Varchar2);
        param.Value = password;
        comm.Parameters.Add(param);

        param = new OracleParameter("accountID", OracleDbType.Int32);
        param.Value = accountID;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }
    }
}