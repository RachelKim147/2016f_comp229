﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for recipe
/// </summary>
public class Recipe
{
    public Recipe()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private String title;

    public String Title
    {
        get { return title; }
        set { title = value; }
    }

    private String submitter;

    public String Submitter
    {
        get { return submitter; }
        set { submitter = value; }
    }

    private int category;

    public int Category
    {
        get { return category; }
        set { category = value; }
    }

    private double cookingTime;
    public double CookingTime
    {
        get { return cookingTime; }
        set { cookingTime = value; }
    }

    private int numOfServing;
    public int NumOfServing
    {
        get { return numOfServing; }
        set { numOfServing = value; }
    }

    private String description;
    public String Description
    {
        get { return description; }
        set { description = value; }
    }

    private int recipeID;
    public int RecipeID
    {
        get { return recipeID; }
        set { recipeID = value; }
    }

}