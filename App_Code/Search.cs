﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Search
/// </summary>
public class Search
{
    public Search()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private int recipeID;

    public int RecipeID
    {
        get { return recipeID; }
        set { recipeID = value; }
    }

    private string name;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    private double cookingTime;

    public double CookingTime
    {
        get { return cookingTime; }
        set { cookingTime = value; }
    }

    private string submitter;

    public string Submitter
    {
        get { return submitter; }
        set { submitter = value; }
    }
}