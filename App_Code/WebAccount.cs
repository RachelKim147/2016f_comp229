﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WebAccount
/// </summary>
public class WebAccount
{
    public WebAccount()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private int accountID;
    public int AccountID
    {
        get { return accountID; }
        set { accountID = value; }
    }

    private string accountName;
    public string AccountName
    {
        get { return accountName; }
        set { accountName = value; }
    }

    private string accountPassword;
    public string AccountPassword
    {
        get { return accountPassword; }
        set { accountPassword = value;  }
    }
}