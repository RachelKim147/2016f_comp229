﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Oracle.ManagedDataAccess.Types;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using System.Data;

/// <summary>
/// Summary description for RecipeRepository
/// </summary>
public class RecipeRepository
{
    public RecipeRepository()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static List<Recipe> GetRecipes()
    {
        List<Recipe> list = new List<Recipe>();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return list;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT * FROM recipes";

        DataTable table = new DataTable();

        try
        {
            // Open the data connection
            comm.Connection.Open();
            // Excute the command and save the result in a DataTanble
            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                Recipe newItem = new Recipe
                {
                    Title = row["recipe_name"].ToString(),
                    Submitter = row["submitter_name"].ToString(),
                    Category = Convert.ToInt32(row["category_id"]),
                    CookingTime = Convert.ToDouble(row["cooking_time"]),
                    NumOfServing = Convert.ToInt32(row["number_servings"]),
                    Description = row["recipe_description"].ToString(),
                    RecipeID = Convert.ToInt32(row["recipe_id"])
                };
                list.Add(newItem);
            }

            // Close the reader
            reader.Close();
        }
        catch (Exception e)
        {
            // [Todo] How can I handle this error?
            string error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }

        return list;
    }

    public static int InsertRecipe(Recipe newItem)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return -1;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandText = "InsertRecipe"; 

        OracleParameter outParam = new OracleParameter("createdID", OracleDbType.Decimal);
        outParam.Direction = ParameterDirection.ReturnValue;
        comm.Parameters.Add(outParam);

        OracleParameter param = new OracleParameter("recpName", OracleDbType.Varchar2);
        param.Value = newItem.Title;
        comm.Parameters.Add(param);

        param = new OracleParameter("submitterName", OracleDbType.Varchar2);
        param.Value = newItem.Submitter;
        comm.Parameters.Add(param);

        param = new OracleParameter("categoryID", OracleDbType.Decimal);
        param.Value = newItem.Category;
        comm.Parameters.Add(param);

        param = new OracleParameter("cookingTime", OracleDbType.Double);
        param.Value = newItem.CookingTime;
        comm.Parameters.Add(param);

        param = new OracleParameter("serving", OracleDbType.Decimal);
        param.Value = newItem.NumOfServing;
        comm.Parameters.Add(param);

        param = new OracleParameter("description", OracleDbType.Varchar2);
        param.Value = newItem.Description;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            // [Todo] How can I handle this error?
            string error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }

        int returnVal = 0;
        if (outParam.Value != null)
        {
            int.TryParse(outParam.Value.ToString(), out returnVal);
        }

        return returnVal;
    }

    public static void DeleteRecipeByRecipeId(int recipeID)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "DELETE FROM recipes WHERE recipe_id = :recipeID";

        OracleParameter param = new OracleParameter("reciperID", OracleDbType.Int32);
        param.Value = recipeID;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            // [Todo] How can I handle this error?
            string error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }
    }

    public static void UpdateRecipe(Recipe updateRecipe)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "UPDATE recipes SET category_id = :categoryID, recipe_name = :recipeName, cooking_time = :cookingTime, number_servings = :serving, recipe_description = :description WHERE recipe_id = :id";

        OracleParameter param = new OracleParameter("categoryID", OracleDbType.Int32);
        param.Value = updateRecipe.Category;
        comm.Parameters.Add(param);

        param = new OracleParameter("recipeName", OracleDbType.Varchar2);
        param.Value = updateRecipe.Title;
        comm.Parameters.Add(param);

        param = new OracleParameter("cookingTime", OracleDbType.Double);
        param.Value = updateRecipe.CookingTime;
        comm.Parameters.Add(param);

        param = new OracleParameter("serving", OracleDbType.Int32);
        param.Value = updateRecipe.NumOfServing;
        comm.Parameters.Add(param);

        param = new OracleParameter("description", OracleDbType.Varchar2);
        param.Value = updateRecipe.Description;
        comm.Parameters.Add(param);

        param = new OracleParameter("id", OracleDbType.Int32);
        param.Value = updateRecipe.RecipeID;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            // [Todo] How can I handle this error?
            string error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }
    }

    public static Recipe GetRecipeByRecipeID(int recipeID)
    {
        Recipe newRecipe = new Recipe();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return newRecipe;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT * FROM recipes WHERE recipe_id = :recipeID";

        OracleParameter param = new OracleParameter("reciperID", OracleDbType.Int32);
        param.Value = recipeID;
        comm.Parameters.Add(param);

        DataTable table = new DataTable();

        try
        {
            comm.Connection.Open();

            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                newRecipe.Title = row["recipe_name"].ToString();
                newRecipe.Submitter = row["submitter_name"].ToString();
                newRecipe.Category = Convert.ToInt32(row["category_id"]);
                newRecipe.CookingTime = Convert.ToDouble(row["cooking_time"]);
                newRecipe.NumOfServing = Convert.ToInt32(row["number_servings"]);
                newRecipe.Description = row["recipe_description"].ToString();
                newRecipe.RecipeID = Convert.ToInt32(row["recipe_id"]);
            }

            reader.Close();
        }
        catch (Exception e)
        {
            // [Todo] How can I handle this error?
            string error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }

        return newRecipe;
    }

    public static DataTable GetRecipeDataTableByRecipeID(int recipeID)
    {
        DataTable table = new DataTable();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return table;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT * FROM recipes WHERE recipe_id = :recipeID";

        OracleParameter param = new OracleParameter("reciperID", OracleDbType.Int32);
        param.Value = recipeID;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();

            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            reader.Close();
        }
        catch (Exception e)
        {
            // [Todo] How can I handle this error?
            string error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }

        return table;
    }
}