﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Ingredient
/// </summary>
public class Ingredient
{
    public Ingredient()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private int ingredientID;
    public int IngredientID
    {
        get { return ingredientID; }
        set { ingredientID = value; }
    }

    private String name;
    public String Name
    {
        get { return name; }
        set { name = value; }
    }

    private double quantity;
    public double Quantity
    {
        get { return quantity; }
        set { quantity = value; }
    }

    private String measure;
    public String Measure
    {
        get { return measure; }
        set { measure = value; }
    }
}