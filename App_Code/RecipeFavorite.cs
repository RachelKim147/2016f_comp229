﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RecipeFavorite
/// </summary>
public class RecipeFavorite
{
    public RecipeFavorite()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private int favoriteID;
    public int FavoriteID
    {
        set { favoriteID = value; }
        get { return favoriteID; }
    }

    private int recipeID;
    public int RecipeID
    {
        set { recipeID = value; }
        get { return recipeID; }
    }
}