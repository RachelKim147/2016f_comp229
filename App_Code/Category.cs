﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Category
/// </summary>
public class Category
{
    private int categoryID;

    public int CategoryID
    {
        get { return categoryID; }
        set { categoryID = value; }
    }

    private string categoryName;

    public string CategoryName
    {
        get { return categoryName; }
        set { categoryName = value; }
    }
    // Constructor
    public Category()
    {

    }

    public Category(int id, string name)
    {
        categoryID = id;
        categoryName = name;
    }
}