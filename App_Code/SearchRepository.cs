﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SearchRepository
/// </summary>
public class SearchRepository
{
    public SearchRepository()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static List<Ingredient> GetIngredientsGroupByName()
    {
        List<Ingredient> list = new List<Ingredient>();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return list;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT upper(INGREDIENT_NAME) FROM INGREDIENTS GROUP BY upper(INGREDIENT_NAME)";

        DataTable table = new DataTable();

        string error;
        try
        {
            comm.Connection.Open();
            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                Ingredient newItem = new Ingredient();
                newItem.Name = row[0].ToString();
                list.Add(newItem);
            }
        }
        catch (Exception e)
        {
            // [TODO] How to show this error message?
            error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }

        return list;
    }

    public static List<Recipe> GetRecipesGroupBySubmitter()
    {
        List<Recipe> list = new List<Recipe>();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return list;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT UPPER(SUBMITTER_NAME) FROM RECIPES GROUP BY UPPER(SUBMITTER_NAME)";

        DataTable table = new DataTable();

        String error;

        try
        {
            // Open the data connection
            comm.Connection.Open();
            // Excute the command and save the result in a DataTanble
            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                Recipe newItem = new Recipe
                {
                    Submitter = row[0].ToString()
                };
                list.Add(newItem);
            }

            // Close the reader
            reader.Close();
        }
        catch (Exception e)
        {
            // [Todo] How can I handle this error?
            error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }

        return list;
    }

    public static List<Search> GetSearchResult(string submitter, int category, string ingredient)
    {
        List<Search> list = new List<Search>();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return list;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        string submitterCheck = " upper(Submitter_Name) = upper(:submitter) ";
        string categoryCheck = " Category_ID = :category ";
        string ingredientCheck = " upper(ingredient_name) = upper(:ingredient) ";
        string groupBy = "GROUP BY RECIPE_ID, Recipe_name, Submitter_Name, Cooking_time";

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT recipe_id, Recipe_name, Submitter_Name, Cooking_time FROM RECIPES JOIN INGREDIENTS USING(Recipe_id) ";

        bool bNeedAnd = false;
        if (submitter != null && string.Equals(submitter, "all", StringComparison.CurrentCultureIgnoreCase)== false)
        {
            if (bNeedAnd == false)
                comm.CommandText += "WHERE";
            comm.CommandText += submitterCheck;
            bNeedAnd = true;

            OracleParameter param = new OracleParameter("submitter", OracleDbType.Varchar2);
            param.Value = submitter;
            comm.Parameters.Add(param);
        }

        if(category > 0)
        {
            if (bNeedAnd == false)
                comm.CommandText += "WHERE";
            else
                comm.CommandText += "AND";
            comm.CommandText += categoryCheck;
            bNeedAnd = true;

            OracleParameter param = new OracleParameter("category", OracleDbType.Int32);
            param.Value = category;
            comm.Parameters.Add(param);
        }

        if(ingredient != null && string.Equals(ingredient, "all", StringComparison.CurrentCultureIgnoreCase) == false)
        {
            if (bNeedAnd == false)
                comm.CommandText += "WHERE";
            else
                comm.CommandText += "AND";
            comm.CommandText += ingredientCheck;
            bNeedAnd = true;

            OracleParameter param = new OracleParameter("ingredient", OracleDbType.Varchar2);
            param.Value = ingredient;
            comm.Parameters.Add(param);
        }

        comm.CommandText += groupBy;

        DataTable table = new DataTable();

        string error;

        try
        {
            // Open the data connection
            comm.Connection.Open();
            // Excute the command and save the result in a DataTanble
            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                Search newItem = new Search()
                {
                    RecipeID = Convert.ToInt32(row["recipe_id"]),
                    Name = row["Recipe_name"].ToString(),
                    Submitter = row["Submitter_Name"].ToString(),
                    CookingTime = Convert.ToDouble(row["Cooking_time"])
                };
                list.Add(newItem);
            }

            // Close the reader
            reader.Close();
        }
        catch (Exception e)
        {
            // [TODO] How to show this error message?
            error = e.Message;
            throw new Exception(error);
        }
        finally
        {
            conn.Close();
        }

        return list;
    }
}