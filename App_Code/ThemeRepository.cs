﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

/// <summary>
/// Summary description for ThemeManager
/// </summary>
public class ThemeRepository
{
    public static List<Theme> GetThemes()
    {
        // Get all themes info rom APP_Themes folder.
        DirectoryInfo dInfo = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("App_Themes"));
        DirectoryInfo[] dArrInfo = dInfo.GetDirectories();
        List<Theme> list = new List<Theme>();
        foreach (DirectoryInfo sDirectory in dArrInfo)
        {
            Theme temp = new Theme(sDirectory.Name);
            list.Add(temp);
        }
        return list;
    }
}