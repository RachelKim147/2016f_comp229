﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RecipeFavoriteRepository
/// </summary>
public class RecipeFavoriteRepository
{
    public RecipeFavoriteRepository()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static List<RecipeFavorite> GetFavoritesByAccountID(int accountID)
    {
        List<RecipeFavorite> list = new List<RecipeFavorite>();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return list;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT * FROM FAVORITES WHERE account_id = :accountID";

        OracleParameter param = new OracleParameter("accountID", OracleDbType.Int32);
        param.Value = accountID;
        comm.Parameters.Add(param);

        DataTable table = new DataTable();

        try
        {
            comm.Connection.Open();

            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                RecipeFavorite favorite = new RecipeFavorite();

                favorite.FavoriteID = Convert.ToInt32(row["favorite_id"]);
                favorite.RecipeID = Convert.ToInt32(row["recipe_id"]);

                list.Add(favorite);
            }

            reader.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }

        return list;
    }

    public static RecipeFavorite GetFavorite(int accountID, int recipeID)
    {
        RecipeFavorite favorite = new RecipeFavorite();

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return favorite;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "SELECT * FROM FAVORITES WHERE account_id = :accountID AND recipe_id = :recipeID";

        OracleParameter param = new OracleParameter("accountID", OracleDbType.Int32);
        param.Value = accountID;
        comm.Parameters.Add(param);

        param = new OracleParameter("reciperID", OracleDbType.Int32);
        param.Value = recipeID;
        comm.Parameters.Add(param);

        DataTable table = new DataTable();

        try
        {
            comm.Connection.Open();

            OracleDataReader reader = comm.ExecuteReader();
            table.Load(reader);

            foreach (DataRow row in table.Rows) // Loop over the rows.
            {
                favorite.FavoriteID = Convert.ToInt32(row["favorite_id"]);
                favorite.RecipeID = Convert.ToInt32(row["recipe_id"]);
            }

            reader.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }

        return favorite;
    }

    public static int InsertFavorite(int accountID, int recipeID)
    {
        int createID = -1;

        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return createID;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandText = "InsertFavorite";

        OracleParameter outParam = new OracleParameter("createdID", OracleDbType.Decimal);
        outParam.Direction = ParameterDirection.ReturnValue;
        comm.Parameters.Add(outParam);

        OracleParameter param = new OracleParameter("accountID", OracleDbType.Decimal);
        param.Value = accountID;
        comm.Parameters.Add(param);

        param = new OracleParameter("recipeID", OracleDbType.Decimal);
        param.Value = recipeID;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }

        if (outParam.Value != null)
        {
            int.TryParse(outParam.Value.ToString(), out createID);
        }

        return createID;
    }

    public static void DeleteFavorite(int accountID, int recipeID)
    {
        if (ConfigurationManager.ConnectionStrings["ConnectionString"] == null)
            return;

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OracleConnection conn = new OracleConnection(connectionString);
        OracleCommand comm = conn.CreateCommand();

        comm.CommandType = CommandType.Text;
        comm.CommandText = "DELETE FROM FAVORITES WHERE account_id = :accountID AND recipe_id = :recipeID";

        OracleParameter param = new OracleParameter("accountID", OracleDbType.Int32);
        param.Value = accountID;
        comm.Parameters.Add(param);

        param = new OracleParameter("recipeID", OracleDbType.Int32);
        param.Value = recipeID;
        comm.Parameters.Add(param);

        try
        {
            comm.Connection.Open();
            comm.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Close();
        }
    }
}