﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AddRecipe.aspx.cs" Inherits="AddRecipe" %>

<%@ Register Src="~/IngredientUserControl.ascx" TagPrefix="uc1" TagName="IngredientUserControl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuCntHolder" Runat="Server">
    <img src="images/add_recipe_title_image.jpg" width="1050" height="406" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyCntHolder" Runat="Server">
    <p class="menuLabel">Add recipes</p>
    <asp:Label ID="lb_nameOfRecipe" runat="server" CssClass="addRecipeBigText" >Name of recipe:</asp:Label>
    <br />
    <asp:TextBox ID="tb_nameOfRecipe" runat="server" Width="400px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="val_nameOfRecipe" runat="server" ErrorMessage="[Mandatory Field] Please put the name of recipe!" ControlToValidate="tb_nameOfRecipe" Display="Dynamic" EnableClientScript="False" ValidationGroup="submit" CssClass="addRecipeError">*</asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Label ID="lb_submittedBy" runat="server" CssClass="addRecipeBigText" >Submitted By:</asp:Label>
    <br />
    <asp:TextBox ID="tb_submittedBy" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
    <asp:RequiredFieldValidator ID="val_submittedBy" runat="server" ErrorMessage="[Mandatory Field] Please put the name of submitter!" ControlToValidate="tb_submittedBy" Display="Dynamic" EnableClientScript="False" ValidationGroup="submit" CssClass="addRecipeError">*</asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Label ID="lb_category" runat="server" CssClass="addRecipeBigText" >Category:</asp:Label>
    <br />
    <asp:DropDownList ID="dd_category" runat="server" DataSourceID="od_category" DataTextField="CategoryName" DataValueField="CategoryID"></asp:DropDownList>
    <asp:ObjectDataSource ID="od_category" runat="server" SelectMethod="GetCategories" TypeName="CategoryRepository"></asp:ObjectDataSource>
    <br />
    <asp:CheckBox ID="check_newCategory" runat="server" CssClass="addRecipeMediumText" Text="Add new category: " AutoPostBack="True" OnCheckedChanged="check_newCategory_CheckedChanged" />
    <asp:TextBox ID="tb_newCategory" runat="server" Enabled="False"></asp:TextBox>
    <asp:RequiredFieldValidator ID="val_newCategory" runat="server" ControlToValidate="tb_newCategory" CssClass="addRecipeError" Display="Dynamic" Enabled="False" ErrorMessage="[Mandatory Field] Please put the new category name!" ValidationGroup="submit" EnableClientScript="False">*</asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Label ID="lb_cookingTime" runat="server" CssClass="addRecipeBigText" >Prepare/Cooking Time:</asp:Label>
    <br />
    <asp:TextBox ID="tb_cookingTime" runat="server" Width="60px"></asp:TextBox>
    <asp:Label ID="lb_cookingTimeMin" runat="server" Text="(hours)" CssClass="addRecipeMediumText"></asp:Label>
    <br />
    <br />
    <asp:Label ID="lb_numOfServing" runat="server" CssClass="addRecipeBigText" >Number of Servings:</asp:Label>
    <br />
    <asp:TextBox ID="tb_numOfServing" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="val_numOfServing" runat="server" Display="Dynamic" ErrorMessage="[Mandatory Field] Please put the number of servings!" ControlToValidate="tb_numOfServing" EnableClientScript="False" ValidationGroup="submit" CssClass="addRecipeError">*</asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Label ID="lb_description" runat="server" CssClass="addRecipeBigText" >Recipe description:</asp:Label>
    <br />
    <asp:TextBox ID="tb_description" runat="server" TextMode="MultiLine" Width="500px" Height="210px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="val_description" runat="server" Display="Dynamic" ErrorMessage="[Mandatory Field] Please put the recipe description!" ControlToValidate="tb_description" EnableClientScript="False" ValidationGroup="submit" CssClass="addRecipeError">*</asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Label ID="lb_listOfIngredients" runat="server" CssClass="addRecipeBigText" >List of ingredients:</asp:Label>
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient1" bChecked="False" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient2" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient3" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient4" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient5" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient6" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient7" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient8" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient9" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient10" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient11" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient12" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient13" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient14" />
    <br />
    <uc1:IngredientUserControl runat="server" ID="ingredient15" />
    <br />
    <br />
    <asp:ValidationSummary ID="val_errorSummary" runat="server" ValidationGroup="submit" CssClass="addRecipeError" />
    <br />
    <asp:Button ID="bt_save" runat="server" Text="Save" OnClick="bt_save_Click" ValidationGroup="submit" />
    <asp:Button ID="br_cancel" runat="server" Text="Cancel" OnClick="br_cancel_Click" />
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="BottomCntHolder" Runat="Server">
</asp:Content>

