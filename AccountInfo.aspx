﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AccountInfo.aspx.cs" Inherits="AccountInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuCntHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyCntHolder" Runat="Server">
    <p class="menuLabel">Account Information</p>
    <table>
        <tr>
            <td>
                <asp:Label ID="lbID" runat="server" Text="ID:" CssClass="defaultStyle"></asp:Label></td>
            <td>
                <asp:TextBox ID="tbID" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbPassword" runat="server" Text="Password:" CssClass="defaultStyle"></asp:Label></td>
            <td>
                <asp:TextBox ID="tbPassword" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="val_pw" runat="server" ErrorMessage="Put password!" CssClass="addRecipeError" Display="Dynamic" ValidationGroup="submit" ControlToValidate="tbPassword" EnableClientScript="False">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btSubmit" runat="server" Text="Submit" OnClick="btSubmit_Click" ValidationGroup="submit" />
            </td>
            <td>
                <asp:Button ID="btLogout" runat="server" Text="Logout" OnClick="btLogout_Click" />
            </td>
            <td>
                <asp:Button ID="btUnregister" runat="server" Text="Unregister" OnClick="btUnregister_Click" ValidationGroup="unregister" />
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="val_submit" runat="server" ValidationGroup="submit" CssClass="addRecipeError" />
    <asp:ValidationSummary ID="val_unregister" runat="server" CssClass="addRecipeError" ValidationGroup="unregister" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="BottomCntHolder" Runat="Server">
</asp:Content>

