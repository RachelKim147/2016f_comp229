﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RecipeDetails : BasePage
{
    // selected recipeID from "Recipe" page.
    private int selectedRecipeID;

    protected void Page_Load(object sender, EventArgs e)
    {
        // [Caution] Don't put this code in checking postback!
        String value = Request["selectedValue"];
        int.TryParse(value, out selectedRecipeID);

        if (!Page.IsPostBack)
        {
            BindRecipeDetails();
            BindIngredientDetails();

            SetFavorite();
        }
    }

    protected void BindIngredientDetails()
    {
        try
        {
            dv_ingredients.DataSource = IngredientRepository.GetIngredientsByRecipeID(selectedRecipeID);
            dv_ingredients.DataKeyNames = new string[] { "IngredientID" };
            dv_ingredients.DataBind();
        }
        catch (Exception e)
        {
            lbResultIngredient.Text = e.Message;
        }
    }

    protected void BindRecipeDetails()
    {
        try
        {
            // [Todo] Why isn't working with List<Recipe> list which contains one item?
            dv_recipe.DataSource = RecipeRepository.GetRecipeDataTableByRecipeID(selectedRecipeID);
            dv_recipe.DataKeyNames = new string[] { "Recipe_ID" };
            dv_recipe.DataBind();
        }
        catch (Exception e)
        {
            lbResultRecipe.Text = e.Message;
        }
    }

    protected void dv_ingredients_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        if (!CheckDeletePermission())
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            switch (e.NewMode)
            {
                case DetailsViewMode.Edit:
                    cv.ErrorMessage = "Only writer can update the ingredient.";
                    break;
                case DetailsViewMode.Insert:
                    cv.ErrorMessage = "Only writer can insert new ingredient.";
                    break;
            }

            this.Page.Validators.Add(cv);
        }
        else
        {
            dv_ingredients.ChangeMode(e.NewMode);
            BindIngredientDetails();
        }
    }

    protected void dv_ingredients_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
    {
        int newPageIndex = e.NewPageIndex;
        dv_ingredients.PageIndex = newPageIndex;

        BindIngredientDetails();
    }

    protected void dv_ingredients_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
    {
        if (!CheckDeletePermission())
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = "Only writer can remove the ingredient.";
            this.Page.Validators.Add(cv);
        }
        else if (dv_ingredients.DataItemCount <= 1) // To prevent remove all ingredients.
        {
            e.Cancel = true;

            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = "There must be at least one ingredient.";
            this.Page.Validators.Add(cv);
        }
        else
        {
            try
            {
                int ingredientID = (int)e.Keys["IngredientID"];
                IngredientRepository.DeleteIngredientByIngredientID(ingredientID);
                BindIngredientDetails();
            }
            catch (Exception error)
            {
                CustomValidator cv = new CustomValidator();
                cv.IsValid = false;
                cv.ErrorMessage = error.Message;
                this.Page.Validators.Add(cv);
            }
        }
    }

    protected void dv_ingredients_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        try
        {
            TextBox newNameTextBox = (TextBox)dv_ingredients.FindControl("tb_edit_name");
            TextBox newQuantityTextBox = (TextBox)dv_ingredients.FindControl("tb_edit_quantity");
            TextBox newMeasureTextBox = (TextBox)dv_ingredients.FindControl("tb_edit_measure");

            Ingredient updateItem = new Ingredient();

            int updateID = -1;
            int.TryParse(dv_ingredients.DataKey.Value.ToString(), out updateID);
            updateItem.IngredientID = updateID;

            updateItem.Name = newNameTextBox.Text;

            int updateQuantity = -1;
            int.TryParse(newQuantityTextBox.Text, out updateQuantity);
            updateItem.Quantity = updateQuantity;

            updateItem.Measure = newMeasureTextBox.Text;

            IngredientRepository.UpdateIngredient(updateItem);
        }
        catch (Exception error)
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = error.Message;
            this.Page.Validators.Add(cv);
        }

        dv_ingredients.ChangeMode(DetailsViewMode.ReadOnly);
        BindIngredientDetails();
    }

    protected void dv_ingredients_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        try
        {
            TextBox newNameTextBox = (TextBox)dv_ingredients.FindControl("tb_insert_name");
            TextBox newQuantityTextBox = (TextBox)dv_ingredients.FindControl("tb_insert_quantity");
            TextBox newMeasureTextBox = (TextBox)dv_ingredients.FindControl("tb_insert_measure");

            Ingredient updateItem = new Ingredient();

            updateItem.Name = newNameTextBox.Text;

            int updateQuantity = -1;
            int.TryParse(newQuantityTextBox.Text, out updateQuantity);
            updateItem.Quantity = updateQuantity;

            updateItem.Measure = newMeasureTextBox.Text;

            IngredientRepository.InsertIngredient(selectedRecipeID, updateItem);
        }
        catch (Exception error)
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = error.Message;
            this.Page.Validators.Add(cv);
        }

        dv_ingredients.ChangeMode(DetailsViewMode.ReadOnly);
        BindIngredientDetails();
    }

    protected void checkBox_edit_newCategory_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox newCategoryCheckBox = (CheckBox)dv_recipe.FindControl("checkBox_edit_newCategory");
            DropDownList categoryDropDown = (DropDownList)dv_recipe.FindControl("dd_edit_category");
            TextBox newCategoryTextBox = (TextBox)dv_recipe.FindControl("tb_edit_newCategory");
            RequiredFieldValidator newCategoryVal = (RequiredFieldValidator)dv_recipe.FindControl("val_newCategory");

            if (newCategoryCheckBox.Checked)
            {
                categoryDropDown.Enabled = false;
                newCategoryTextBox.Enabled = true;
                newCategoryVal.Enabled = true;
            }
            else
            {
                categoryDropDown.Enabled = true;
                newCategoryTextBox.Text = "";
                newCategoryTextBox.Enabled = false;
                newCategoryVal.Enabled = false;
            }
        }
        catch (Exception error)
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = error.Message;
            this.Page.Validators.Add(cv);
        }     
    }

    protected void dv_recipe_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        if (!CheckDeletePermission())
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            switch(e.NewMode)
            {
                case DetailsViewMode.Edit:
                    cv.ErrorMessage = "Only writer can update the recipe.";
                    break;
                case DetailsViewMode.Insert:
                    cv.ErrorMessage = "Only writer can insert new recipe.";
                    break;
            }
            
            this.Page.Validators.Add(cv);
        }
        else
        {
            dv_recipe.ChangeMode(e.NewMode);
            BindRecipeDetails();
        }      
    }

    protected void dv_recipe_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
    {
        if (CheckDeletePermission())
        {
            RecipeRepository.DeleteRecipeByRecipeId(selectedRecipeID);
            Response.Redirect("Recipes.aspx");
        }
        else
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = "Only writer can remove the recipe.";
            this.Page.Validators.Add(cv);
        }
    }

    protected void dv_recipe_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        try
        {
            CheckBox newCategoryCheckBox = (CheckBox)dv_recipe.FindControl("checkBox_edit_newCategory");
            DropDownList categoryDropDown = (DropDownList)dv_recipe.FindControl("dd_edit_category");
            TextBox newCategoryTextBox = (TextBox)dv_recipe.FindControl("tb_edit_newCategory");
            TextBox newTitleTextBox = (TextBox)dv_recipe.FindControl("tb_edit_title");
            TextBox newTimeTextBox = (TextBox)dv_recipe.FindControl("tb_edit_time");
            TextBox newServingTextBox = (TextBox)dv_recipe.FindControl("tb_edit_serving");
            TextBox newDescriptionTextBox = (TextBox)dv_recipe.FindControl("tb_edit_description");

            int categoryID = -1;

            if (newCategoryCheckBox.Checked)
            {
                categoryID = CategoryRepository.InsertCategory(newCategoryTextBox.Text);
            }
            else
            {
                categoryID = Convert.ToInt32(categoryDropDown.SelectedValue);
            }

            Recipe updateItem = new Recipe
            {
                Title = newTitleTextBox.Text,
                Category = categoryID,
                CookingTime = Convert.ToDouble(newTimeTextBox.Text),
                NumOfServing = Convert.ToInt32(newServingTextBox.Text),
                Description = newDescriptionTextBox.Text,
                RecipeID = selectedRecipeID
            };

            RecipeRepository.UpdateRecipe(updateItem);
        }
        catch (Exception error)
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = error.Message;
            this.Page.Validators.Add(cv);
        }

        dv_recipe.ChangeMode(DetailsViewMode.ReadOnly);
        BindRecipeDetails();
    }

    protected bool CheckDeletePermission()
    {
        bool bEquals = false;

        try
        {
            Label submitterTB = (Label)dv_recipe.FindControl("lb_submitter");
            string nameInDV = submitterTB.Text;
            string nameInSession = User.Identity.Name;
            bEquals = nameInDV.Equals(nameInSession, StringComparison.OrdinalIgnoreCase);
        }
        catch (Exception error)
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = error.Message;
            this.Page.Validators.Add(cv);
        }

        return bEquals;
    }

    protected void cb_favorite_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            WebAccount myInfo = WebAccountRepository.GetAccountByAccountName(User.Identity.Name);
            if (cb_favorite.Checked)
            {
                RecipeFavoriteRepository.InsertFavorite(myInfo.AccountID, selectedRecipeID);
            }
            else
            {
                RecipeFavoriteRepository.DeleteFavorite(myInfo.AccountID, selectedRecipeID);
            }
        }
        catch (Exception error)
        {
            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = error.Message;
            this.Page.Validators.Add(cv);
        }
    }

    protected void SetFavorite()
    {
        try
        {
            WebAccount myInfo = WebAccountRepository.GetAccountByAccountName(User.Identity.Name);
            RecipeFavorite myFavo = RecipeFavoriteRepository.GetFavorite(myInfo.AccountID, selectedRecipeID);

            if(selectedRecipeID != 0 && myFavo.RecipeID == selectedRecipeID)
            {
                cb_favorite.Checked = true;
            }
            else
            {
                cb_favorite.Checked = false;
            }
        }
        catch (Exception error)
        {
            cb_favorite.Checked = false;

            CustomValidator cv = new CustomValidator();
            cv.IsValid = false;
            cv.ErrorMessage = error.Message;
            this.Page.Validators.Add(cv);
        }
    }
}