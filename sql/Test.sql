DROP TABLE INGREDIENTS;
DROP TABLE ACCOUNTS;
DROP TABLE CATEGORIES;
DROP TABLE RECIPES;
DROP TABLE FAVORITES;

DROP PROCEDURE InsertIngredient;
DROP FUNCTION InsertRecipe;
DROP FUNCTION insertCategory;
DROP FUNCTION InsertAccount;
DROP FUNCTION InsertCategory;

CREATE TABLE CATEGORIES
  (category_id NUMBER(2),
  category_name VARCHAR2(20),
  CONSTRAINT categories_id_pk PRIMARY KEY (category_id) );

CREATE TABLE ACCOUNTS
  ( account_id NUMBER(6),
    account_name VARCHAR2(50),
    account_password VARCHAR2(50),
    CONSTRAINT account_id_pk PRIMARY KEY (account_id),
    CONSTRAINT account_name_unq UNIQUE (account_name) );
  
CREATE TABLE RECIPES
  ( recipe_id NUMBER(6),
  category_id NUMBER(2),
  recipe_name VARCHAR2(50),
  submitter_name VARCHAR2(50),
  cooking_time NUMBER(6,2),
  number_servings NUMBER(3),
  recipe_description VARCHAR2(2000),
  CONSTRAINT recipes_id_pk PRIMARY KEY (recipe_id),
  CONSTRAINT recipes_categoryId_fk FOREIGN KEY (category_id)
    REFERENCES categories(category_id) ON DELETE CASCADE,
  CONSTRAINT recipes_submitter_fk FOREIGN KEY (submitter_name)
    REFERENCES ACCOUNTS(account_name) ON DELETE CASCADE	);
    
CREATE TABLE INGREDIENTS
  ( ingredient_id NUMBER(6),
  recipe_id NUMBER(6),
  ingredient_name VARCHAR2(50),
  quantity NUMBER(6,2),
  measure VARCHAR2(50),
  CONSTRAINT ingredients_id_pk PRIMARY KEY (ingredient_id),
  CONSTRAINT ingredients_recipeId_fk FOREIGN KEY (recipe_id)
    REFERENCES recipes(recipe_id) ON DELETE CASCADE);
    


ALTER TABLE ACCOUNTS MODIFY ("ACCOUNT_NAME" NOT NULL ENABLE);
ALTER TABLE ACCOUNTS MODIFY ("ACCOUNT_PASSWORD" NOT NULL ENABLE);
	
CREATE TABLE FAVORITES
 ( favorite_id NUMBER(6),
   account_id NUMBER(6),
   recipe_id NUMBER(6),
   CONSTRAINT favorite_id_unq UNIQUE (favorite_id),
   CONSTRAINT favorite_id_pk PRIMARY KEY (account_id, recipe_id),
   CONSTRAINT favorites_accountId_fk FOREIGN KEY (account_id)
    REFERENCES ACCOUNTS(account_id) ON DELETE CASCADE,
   CONSTRAINT favorites_recipeId_fk FOREIGN KEY (recipe_id)
    REFERENCES RECIPES(recipe_id) ON DELETE CASCADE )
	
INSERT INTO CATEGORIES
  VALUES (1, 'Breakfast');
INSERT INTO CATEGORIES 
  VALUES (2, 'Lunch');
INSERT INTO CATEGORIES 
  VALUES(3, 'Dinner');
Commit;

INSERT INTO ACCOUNTS
 VALUES(1, 'account1', 'pw1'); 
INSERT INTO ACCOUNTS
 VALUES(2, 'account2', 'pw2'); 
INSERT INTO ACCOUNTS
 VALUES(3, 'account3', 'pw3'); 
INSERT INTO ACCOUNTS
 VALUES(4, 'account4', 'pw4'); 
INSERT INTO ACCOUNTS
 VALUES(5, 'account5', 'pw5'); 
 

INSERT INTO RECIPES
 VALUES (1, 1, 'Pierogies with Sausage, Cabbage and Pear', 'account1', 0.4, 4, 'Cook pierogies according to package directions.\r\nHeat oil in large skillet over medium-high heat. Add bratwurst and cook, breaking into pieces with a wooden spoon, until browned, 6 to 8 minutes. Add cabbage and scallions and cook, stirring occasionally, until just wilted, 5 to 7 minutes. Add pears, vinegar, and mustard and cook until pears are warm, 1 to 2 minutes. Season with salt and pepper.\r\nServe sausage mixture topped with pierogies.' );
INSERT INTO RECIPES
 VALUES (2, 1, 'Thai Noodle Soup with Shrimp and Pumpkin', 'account2', 0.5, 4, 'Cook noodles according to package directions.\r\nMeanwhile, heat oil in a large saucepan over medium-high heat. Add shallot and garlic and cook, stirring, until fragrant, 1 minute. Add ginger and cook, stirring, until fragrant, 1 minute. Add stock, coconut milk, pumpkin, chili paste, and brown sugar. Bring to a boil, reduce heat and simmer, stirring occasionally, until pumpkin is just tender, 10 to 12 minutes. Stir in shrimp and cook until opaque throughout, 1 to 2 minutes. Stir in snow peas and cook until bright green, 1 minute. Stir in lime juice and fish sauce.\r\nDivide noodles between four serving bowls. Top with soup and basil.' );
Commit;

INSERT INTO INGREDIENTS
 VALUES(1, 1, 'box frozen onion pierogies', 16, 'oz' ); 
INSERT INTO INGREDIENTS
 VALUES(2, 1, 'extra-virgin olive oil', 1, 'tbsp' ); 
INSERT INTO INGREDIENTS
 VALUES(3, 1, 'fresh bratwurst, casings removed', 0.5, 'lb' ); 
INSERT INTO INGREDIENTS
 VALUES(4, 1, 'small head green cabbage, shredded', 8, 'oz' ); 
INSERT INTO INGREDIENTS
 VALUES(5, 1, 'scallions, cut into 2" pieces', 5, 'pieces' );
INSERT INTO INGREDIENTS
 VALUES(6, 1, 'firm ripe pear, cored and sliced', 1, 'pieces' );
INSERT INTO INGREDIENTS
 VALUES(7, 1, 'apple cider vinegar', 1.5, 'tbsp' );
INSERT INTO INGREDIENTS
 VALUES(8, 1, 'whole grain mustard', 1, 'tbsp' );
INSERT INTO INGREDIENTS
 VALUES(9, 1, 'Kosher salt', 0, 'proper' );
INSERT INTO INGREDIENTS
 VALUES(10, 1, 'Freshly ground black pepper', 0, 'proper' );

INSERT INTO INGREDIENTS
 VALUES(11, 2, 'Thai rice noodles', 8, 'oz' ); 
INSERT INTO INGREDIENTS
 VALUES(12, 2, 'canola oil', 1, 'tbsp' ); 
INSERT INTO INGREDIENTS
 VALUES(13, 2, 'garlic cloves, chopped', 2, 'pieces' ); 
INSERT INTO INGREDIENTS
 VALUES(14, 2, 'grated fresh ginger', 1, 'tbsp' ); 
INSERT INTO INGREDIENTS
 VALUES(15, 2, 'chicken stock', 2, 'c' );
INSERT INTO INGREDIENTS
 VALUES(16, 2, 'cubed fresh pumpkin or butternut squash', 3, 'c' );
INSERT INTO INGREDIENTS
 VALUES(17, 2, 'Thai roasted red chili paste', 1, 'tbsp' );
INSERT INTO INGREDIENTS
 VALUES(18, 2, 'dark brown sugar', 1, 'tbsp' );
INSERT INTO INGREDIENTS
 VALUES(19, 2, 'large shrimp, peeled and deveined', 1, 'lb' );
INSERT INTO INGREDIENTS
 VALUES(20, 2, 'snow peas', 1, 'c' );
INSERT INTO INGREDIENTS
 VALUES(21, 2, 'fresh lime juice', 2, 'tbsp' );
INSERT INTO INGREDIENTS
 VALUES(22, 2, 'fish sauce', 1, 'tbsp' );
INSERT INTO INGREDIENTS
 VALUES(23, 2, 'Fresh basil, for serving', 0, 'proper' );
 

INSERT INTO FAVORITES
 VALUES(1, 2, 1);
 
 commit;
 
create or replace PROCEDURE InsertIngredient
  ( recID IN NUMBER,
    ingName IN VARCHAR2,
    quan IN NUMBER,
    msr IN VARCHAR2 )
  IS
BEGIN
  INSERT INTO INGREDIENTS 
    VALUES (SEQ_INGREDIENT_ID.NEXTVAL, recID, ingName, quan, msr);
  COMMIT;
END InsertIngredient;
  
CREATE OR REPLACE FUNCTION InsertRecipe
 (recpName IN VARCHAR2,
  submitterName IN VARCHAR2,
  categoryID IN NUMBER,
  cookingTime IN NUMBER,
  serving IN NUMBER,
  description IN VARCHAR2)
 RETURN NUMBER
IS
 createdID RECIPES.recipe_id%TYPE;
BEGIN
  INSERT INTO RECIPES
    VALUES (SEQ_RECIPE_ID.NEXTVAL, categoryID, recpName, submitterName, cookingTime, serving, description)
    RETURNING recipe_id INTO createdID;
  COMMIT;
  RETURN createdID;
END InsertRecipe;

create or replace FUNCTION insertCategory
  ( categoryName IN VARCHAR2 )
  RETURN NUMBER
  IS
  createdid categories.category_id%TYPE;
BEGIN
  INSERT INTO categories 
    VALUES (seq_category_id.nextval, categoryName)
    RETURNING category_id INTO createdid;
  COMMIT;
  RETURN createdID;
END insertCategory;

create or replace FUNCTION InsertAccount
  ( accountName IN VARCHAR2.
	accountPassword IN VARCHAR2 )
  RETURN NUMBER
  IS
  createdid ACCOUNTS.account_id%TYPE;
BEGIN
  INSERT INTO ACCOUNTS 
    VALUES (SEQ_ACCOUNT_ID.nextval, accountName, accountPassword)
    RETURNING account_id INTO createdid;
  COMMIT;
  RETURN createdID;
END InsertAccount;

create or replace FUNCTION InsertFavorite
  ( accountID IN NUMBER.
	recipeID IN NUMBER )
  RETURN NUMBER
  IS
  createdid FAVORITES.favorite_id%TYPE;
BEGIN
  INSERT INTO FAVORITES 
    VALUES (SEQ_FAVORITE_ID.nextval, accountID, recipeID)
    RETURNING favorite_id INTO createdid;
  COMMIT;
  RETURN createdID;
END InsertFavorite;

--------------------------------------------------------
--  DDL for Sequence SEQ_CATEGORY_ID
--------------------------------------------------------
DROP SEQUENCE  SEQ_CATEGORY_ID;
DROP SEQUENCE  SEQ_INGREDIENT_ID;
DROP SEQUENCE  SEQ_RECIPE_ID;
DROP SEQUENCE  SEQ_FAVORITE_ID;
DROP SEQUENCE  SEQ_ACCOUNT_ID;

   CREATE SEQUENCE  SEQ_CATEGORY_ID  MINVALUE 4 MAXVALUE 99 INCREMENT BY 1 START WITH 4 CACHE 20 NOORDER  CYCLE  NOPARTITION ;

   
   CREATE SEQUENCE  SEQ_INGREDIENT_ID  MINVALUE 24 MAXVALUE 999999 INCREMENT BY 1 START WITH 24 NOCACHE  NOORDER  CYCLE  NOPARTITION ;
   
   
   CREATE SEQUENCE  SEQ_RECIPE_ID  MINVALUE 2 MAXVALUE 999999 INCREMENT BY 1 START WITH 3 NOCACHE  NOORDER  CYCLE  NOPARTITION ;
   
   
   CREATE SEQUENCE  SEQ_FAVORITE_ID  MINVALUE 2 MAXVALUE 999999 INCREMENT BY 1 START WITH 2 CACHE 20 NOORDER  CYCLE  NOPARTITION ;

   CREATE SEQUENCE  SEQ_ACCOUNT_ID  MINVALUE 6 MAXVALUE 999999 INCREMENT BY 1 START WITH 6 CACHE 20 NOORDER  CYCLE  NOPARTITION ;

